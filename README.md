# Dotpay Payment Processor for CiviCRM

Information about payment processor: https://www.dotpay.pl (in Polish).

Information about CiviCRM: http://civicrm.org.pl (in Polish), http://civicrm.org (in English).

# Setup instructions

Install the extension as usual (manual installation instructions provided here: https://wiki.civicrm.org/confluence/display/CRMDOC/Extensions#Extensions-Installinganewextension).


Development of this extension was supported by Stefan Batory Foundation (http://www.batory.org.pl/).
Extension developed by Caltha (http://caltha.eu).


# Procesor płatności Dotpay dla CiviCRM

Informacja o dostawcy płatności online: https://www.dotpay.pl

Informacje o CiviCRM: http://civicrm.org.pl, http://civicrm.org (po angielsku).

# Instrukcja instalacji

Standardowa instalacja rozszerzenia (instrukcja manualnej instalacji zgodna z: https://wiki.civicrm.org/confluence/display/CRMDOC/Extensions#Extensions-Installinganewextension).

# Konfiguracja

Poniżej opisano sposób konfiguracji procesora płatności ze środowiskiem testowym Dotpay.

1. Zarejestruj się w środowisku testowym pod adresem [https://ssl.dotpay.pl/test_seller/test/registration/](https://ssl.dotpay.pl/test_seller/test/registration/). Rejestracja jest szybka i wymaga podania tylko adresu email.

2. Zaloguj się na [https://ssl.dotpay.pl/test_seller/](https://ssl.dotpay.pl/test_seller/)

3. Otwórz stronę Ustawienia > Powiadomienia > Konfiguracja urlc > Edycja

	* Skopiuj identyfikator sklepu, jest w linii "Powiązanie: Test User (twoj-adres//email.pl) #XXXXXX", po znaku #
	* Skopiuj PIN
	* Ustaw Urlc: `https://STRONA.PL/civicrm/dotpay/response`

4. Otwórz stronę Ustawienia > Konfiguracja sklepu

	* Ustaw pole Wersja API = `dev`

5. Zaloguj się do CiviCRM i przejdź do strony z procesorami płatności, dodaj nowy procesor:

	* Typ procesora płatności: Dotpay
	* Nazwa: wg uznania, `Mój Procesor Płatności DotPay`
	* Payment Method: `Dotpay`
	* Konto księgowe: proponuję `Payment Processor Account`
	* Użyj tych samych danych do obu sekcji Live i Test:
		* Identyfikator sklepu (#id) = XXXXXX
		* PIN = ten skopiowany
		* adres url = [https://ssl.dotpay.pl/test_payment/](https://ssl.dotpay.pl/test_payment/)

6. Dodaj nową stronę transakcyjną

	* Rodzaj finansowania: `Paid by Dotpay`
	* Odznaczyć `Use a confirmation page`
	* System płatności: `Mój Procesor Płatności DotPay`
	* Pozostałe ustawienia wg potrzeb

7. Otwórz stronę transakcyjną i testuj.

Stworzenie tego rozszerzenia było możliwe dzięki wsparciu Fundacji Stefana Batorego (http://www.batory.org.pl/). Rozszerzenie zostało napisane przez Caltha (http://caltha.eu).
