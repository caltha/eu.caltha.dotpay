<?php

/**
 * Collection of upgrade steps.
 */
class CRM_Dotpay_Upgrader extends CRM_Dotpay_Upgrader_Base {

  public function install() {
    $data = array(
      'payment_instrument' => array(
        'values' => array(
          'dotpay' => array(
            'label' => 'Dotpay',
            'is_default' => 0,
          ),
        ),
      ),
    );
    $options = new CRM_Dotpay_Tools_Options($data);
    $options->install();
  }
}
