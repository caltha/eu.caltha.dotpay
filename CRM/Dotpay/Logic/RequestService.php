<?php

class CRM_Dotpay_Logic_RequestService extends CRM_Dotpay_Logic_Service {

  /**
   * Prepare array with required params for service.
   *
   * @param array $processor Given processor
   * @param array $params
   *
   * @return array
   */
  public static function prepare($processor, $params) {
    $request = array();
    $request['id'] = $processor['user_name'];
    $request['amount'] = $params['amount'];
    $request['currency'] = 'PLN';
    $request['description'] = $params['description'];
    $request['lang'] = 'pl';
    $request['URL'] = CRM_Utils_System::url('civicrm/dotpay/final', array('control' => $params['invoiceID']), true);
    $request['type'] = 3;
    $request['control'] = $params['invoiceID'];
    list($firstName, $lastName) = self::getFirstLastName($params['contactID']);
    $request['firstname'] = $firstName;
    $request['lastname'] = $lastName;
    $request['email'] = self::getEmail($params['contactID']);
    return self::orderFilter($request, CRM_Dotpay_Dicts_Service::$requestKeys);
  }

  /**
   * Add checksum to the end of array.
   *
   * @param array $request
   * @param string $chk
   *
   * @return mixed
   */
  public static function addChk($request, $chk) {
    $request['chk'] = $chk;
    return $request;
  }
}
