<?php

class CRM_Dotpay_Logic_Service {

  /**
   * Add controlData to the end of array.
   *
   * @param $request
   * @param $controlData
   *
   * @return mixed
   */
  public static function addControlData($request, $controlData) {
    $request['controlData'] = $controlData;
    return $request;
  }


  /**
   * Order by required sequence and filter by not empty values.
   *
   * @param array $request
   * @param array $order
   *
   * @return array
   */
  protected static function orderFilter($request, $order) {
    $req = array();
    foreach ($order as $key) {
      if (array_key_exists($key, $request) && !empty($request[$key])) {
        $req[$key] = $request[$key];
      }
    }
    return $req;
  }


  /**
   * Get email by contact id.
   *
   * @param int $contactId
   *
   * @return string
   */
  protected static function getEmail($contactId) {
    $params = array(
      'sequential' => 1,
      'contact_id' => $contactId,
      'is_primary' => 1,
    );
    $result = civicrm_api3('Email', 'get', $params);
    if ($result['count'] == 1) {
      return $result['values'][0]['email'];
    }
    return '';
  }


  /**
   * Get first name and last name by contact id.
   *
   * @param int $contactId
   *
   * @return array first_name and last_name
   */
  protected static function getFirstLastName($contactId) {
    $params = array(
      'sequential' => 1,
      'contact_id' => $contactId,
    );
    $result = civicrm_api3('Contact', 'get', $params);
    if ($result['count'] == 1) {
      return array($result['values'][0]['first_name'], $result['values'][0]['last_name']);
    }
    return array();
  }
}
