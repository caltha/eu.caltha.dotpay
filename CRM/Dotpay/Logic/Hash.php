<?php

class CRM_Dotpay_Logic_Hash {
  /**
   * Calculate SHA256 code from data.
   *
   * @param string $PIN
   * @param array $columns
   *
   * @return string
   */
  public static function calculate($PIN, $columns) {
    unset($columns['PIN']);
    unset($columns['signature']);
    return hash('sha256', $PIN . implode('', $columns));
  }
}
