<?php

class CRM_Dotpay_Tools_Options {

  public $data = array();

  function __construct($data) {
    $this->data = $data;
  }

  public function install() {
    foreach ($this->data as $groupName => $group) {
      $groupId = $this->setGroup($groupName, $group);
      if (is_array($group['values'])) {
        $groupValues = $group['values'];
        $this->setValues($groupId, $groupValues);
      }
    }
  }

  private function setGroup($groupName, $group) {
    $result = civicrm_api3('OptionGroup', 'getsingle', array('version' => 3, 'name' => $groupName));
    if (isset($result['is_error']) && $result['is_error']) {
      $params = array(
        'version' => 3,
        'sequential' => 1,
        'name' => $groupName,
        'is_reserved' => 1,
        'is_active' => 1,
        'title' => $group['title'],
        'description' => $group['description'],
      );
      $result = civicrm_api3('OptionGroup', 'create', $params);
      $groupId = $result['values'][0]['id'];
    } else {
      $groupId = $result['id'];
    }
    return $groupId;
  }

  private function setValues($groupId, $groupValues) {
    foreach ($groupValues as $valueName => $value) {
      $params = array(
        'sequential' => 1,
        'option_group_id' => $groupId,
        'name' => $valueName
      );
      $result = civicrm_api3('OptionValue', 'get', $params);
      if ($result['count'] == 0) {
        $params = array(
          'sequential' => 1,
          'option_group_id' => $groupId,
          'name' => $valueName,
          'label' => $value['label'],
          'is_default' => $value['is_default'],
          'is_active' => 1,
        );
        if (isset($value['value'])) {
          $params['value'] = $value['value'];
        }
        civicrm_api3('OptionValue', 'create', $params);
      }
    }
  }
}
