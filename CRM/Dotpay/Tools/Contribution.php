<?php

class CRM_Dotpay_Tools_Contribution {

  public $result = array();

  function __construct($invoiceId) {
    if ($invoiceId) {
      $query = "SELECT t.*, s.id AS petition_id, s.title AS petition_title
                FROM civicrm_contribution t
                  JOIN civicrm_contact c ON c.id = t.contact_id
                  LEFT JOIN civicrm_survey s ON s.campaign_id = t.campaign_id AND s.activity_type_id = 32
                WHERE t.invoice_id = %1";
      $params = array(
        1 => array($invoiceId, 'String'),
      );
      $dao = CRM_Core_DAO::executeQuery($query, $params);
      $dao->fetch();
      $this->result = $dao;
    }
  }

  public function setCompleted($contributionId, $additionalParams = array()) {
    $param = array(
      'sequential' => 1,
      'id' => $contributionId,
    );
    $param = array_merge($param, $additionalParams);
    $result = civicrm_api3('Contribution', 'completetransaction', $param);
    return !$result['is_error'];
  }

  public function setFailed($contributionId, $additionalParams = array()) {
    return $this->setStatus($contributionId, 4, $additionalParams);
  }

  private function setStatus($contributionId, $statusId, $additionalParams = array()) {
    $param = array(
      'sequential' => 1,
      'id' => $contributionId,
      'contribution_status_id' => $statusId,
    );
    $param = array_merge($param, $additionalParams);
    $result = civicrm_api3('Contribution', 'create', $param);
    return !$result['is_error'];
  }
}
