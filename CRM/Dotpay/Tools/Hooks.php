<?php

class CRM_Dotpay_Tools_Hooks {

  static $null = NULL;

  static function alterPageRun(&$page, $pageClass) {
    return CRM_Utils_Hook::singleton()->invoke(2, $page, $pageClass, self::$null, self::$null, self::$null, self::$null, 'civicrm_dotpayAlterPageRun');
  }
}
