<?php

/**
 * Operation status in URLC
 */
class CRM_Dotpay_Dicts_OperationStatus {
  const NEW_ = 'new',
    PROCESSING = 'processing',
    COMPLETED = 'completed',
    REJECTED = 'rejected',
    PROCESSING_REALIZATION_WAITING = 'processing_realization_waiting',
    PROCESSING_REALIZATION = 'processing_realization';

  public static $ids = array(
    self::NEW_,
    self::PROCESSING,
    self::COMPLETED,
    self::REJECTED,
    self::PROCESSING_REALIZATION_WAITING,
    self::PROCESSING_REALIZATION,
  );

  public static $labels = array(
    self::NEW_ => 'Nowa',
    self::PROCESSING => 'Oczekuje na wpłatę',
    self::COMPLETED => 'Wykonana',
    self::REJECTED => 'Odrzucona',
    self::PROCESSING_REALIZATION_WAITING => 'Oczekuje na realizację',
    self::PROCESSING_REALIZATION => 'Realizowana',
  );
}
