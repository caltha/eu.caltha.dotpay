<?php

/**
 * Operation types in URLC
 */
class CRM_Dotpay_Dicts_OperationType {
  const PAYMENT = 'payment',
    PAYMENT_MULTIMERCHANT_CHILD = 'payment_multimerchant_child',
    PAYMENT_MULTIMERCHANT_PARENT = 'payment_multimerchant_parent',
    REFUND = 'refund',
    PAYOUT = 'payout',
    RELEASE_ROLLBACK = 'release_rollback',
    UNIDENTIFIED_PAYMENT = 'UNIDENTIFIED_PAYMENT',
    COMPLAINT = 'complaint';

  public static $ids = array(
    self::PAYMENT,
    self::PAYMENT_MULTIMERCHANT_CHILD,
    self::PAYMENT_MULTIMERCHANT_PARENT,
    self::REFUND,
    self::PAYOUT,
    self::RELEASE_ROLLBACK,
    self::UNIDENTIFIED_PAYMENT,
    self::COMPLAINT,
  );

  public static $labels = array(
    self::PAYMENT => 'Płatność',
    self::PAYMENT_MULTIMERCHANT_CHILD => 'Płatność multimerchant',
    self::PAYMENT_MULTIMERCHANT_PARENT => 'Nadpłatność multimerchant',
    self::REFUND => 'Zwrot',
    self::PAYOUT => 'Wypłata',
    self::RELEASE_ROLLBACK => 'Zwolnienie rollbacka',
    self::UNIDENTIFIED_PAYMENT => 'Płatność niezidentyfikowana',
    self::COMPLAINT => 'Reklamacja',
  );
}
