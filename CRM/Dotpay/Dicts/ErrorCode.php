<?php

/**
 * Error codes for URL page
 */
class CRM_Dotpay_Dicts_ErrorCode {
  const PAYMENT_EXPIRED = 'PAYMENT_EXPIRED',
    UNKNOWN_CHANNEL = 'UNKNOWN_CHANNEL',
    DISABLED_CHANNEL = 'DISABLED_CHANNEL',
    BLOCKED_ACCOUNT = 'BLOCKED_ACCOUNT',
    INACTIVE_SELLER = 'INACTIVE_SELLER',
    AMOUNT_TOO_LOW = 'AMOUNT_TOO_LOW',
    AMOUNT_TOO_HIGH = 'AMOUNT_TOO_HIGH',
    BAD_DATA_FORMAT = 'BAD_DATA_FORMAT',
    HASH_NOT_EQUAL_CHK = 'HASH_NOT_EQUAL_CHK',
    UNKNOWN_ERROR = 'UNKNOWN_ERROR';

  public static $ids = array(
    self::PAYMENT_EXPIRED,
    self::UNKNOWN_CHANNEL,
    self::DISABLED_CHANNEL,
    self::BLOCKED_ACCOUNT,
    self::INACTIVE_SELLER,
    self::AMOUNT_TOO_LOW,
    self::AMOUNT_TOO_HIGH,
    self::BAD_DATA_FORMAT,
    self::HASH_NOT_EQUAL_CHK,
    self::UNKNOWN_ERROR,
  );

  public static $labels = array(
    self::PAYMENT_EXPIRED => 'Przekroczona data ważności wygenerowanego linku płatniczego lub przekazana w parametrze expiration_date',
    self::UNKNOWN_CHANNEL => 'Nieznany kanał',
    self::DISABLED_CHANNEL => 'Wyłączony kanał płatności',
    self::BLOCKED_ACCOUNT => 'Zablokowane konto',
    self::INACTIVE_SELLER => 'Brak możliwości dokonania płatności spowodowany brakiem aktywacji konta',
    self::AMOUNT_TOO_LOW => 'Kwota mniejsza niż minimalna określona dla sklepu',
    self::AMOUNT_TOO_HIGH => 'Kwota większa niż maksymalna określona dla sklepu',
    self::BAD_DATA_FORMAT => 'Przesłano błędny format danych np. błędny format parametru expiration_date',
    self::HASH_NOT_EQUAL_CHK => 'Błędna wartości parametru chk',
    self::UNKNOWN_ERROR => 'Wartość zwracana w innym przypadku niż powyższe',
  );
}
