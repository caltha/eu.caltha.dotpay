<?php

require_once 'CRM/Core/Page.php';

class CRM_Dotpay_Page_Response extends CRM_Core_Page {

  public $dotPayIP = '195.150.9.37';
  public $id = 0;
  public $control = '';
  public $operationStatus = '';
  public $signature = '';
  public $response = array();

  function run() {
    if (!$this->validRemoteAddress()) {
      $this->handleWarning();
    }
    $this->setValues();
    $contribution = new CRM_Dotpay_Tools_Contribution($this->control);
    if ($contribution->result->id) {
      $query = "SELECT password AS PIN
                FROM civicrm_payment_processor
                WHERE user_name = %1 AND is_test = %2
                LIMIT 1";
      $params = array(
        1 => array($this->id, 'String'),
        2 => array($contribution->result->is_test, 'Integer'),
      );
      $PIN = CRM_Core_DAO::singleValueQuery($query, $params);
      $calculatedHash = CRM_Dotpay_Logic_Hash::calculate($PIN, $this->response);
      if ($this->signature == $calculatedHash) {
        switch ($this->operationStatus) {
          case CRM_Dotpay_Dicts_OperationStatus::COMPLETED:
            $contribution->setCompleted($contribution->result->id, array('fee_amount' => abs($this->response['operation_commission_amount'])));
            break;
          case CRM_Dotpay_Dicts_OperationStatus::REJECTED:
            $contribution->setFailed($contribution->result->id);
            break;
          default:
            CRM_Core_Error::debug_var('RESPONSE UNSUPPORTED $operationStatus', $this->operationStatus);
            CRM_Core_Error::debug_var('RESPONSE UNSUPPORTED $control', $this->control);
        }
      } else {
        CRM_Core_Error::debug_var('INVALID SIGNATURE $signature', $this->signature);
        CRM_Core_Error::debug_var('INVALID SIGNATURE $calculatedHash', $calculatedHash);
        CRM_Core_Error::debug_var('INVALID SIGNATURE $response', $this->response);
      }
    }
    header('HTTP/1.1 200 Ok');
    echo "OK";
    CRM_Utils_System::civiExit();
  }

  private function setValues() {
    $this->id = CRM_Utils_Request::retrieve('id', 'Positive', $this, false);
    $this->control = CRM_Utils_Request::retrieve('control', 'String', $this, false);
    $this->signature = CRM_Utils_Request::retrieve('signature', 'String', $this, false);
    $this->operationStatus = CRM_Utils_Request::retrieve('operation_status', 'String', $this, false);
    $this->response = array();
    foreach (CRM_Dotpay_Dicts_Service::$responseKeys as $key) {
      $this->response[$key] = CRM_Utils_Request::retrieve($key, 'String', $this, false);
    }
  }

  private function validRemoteAddress() {
    return ($_SERVER['REMOTE_ADDR'] == $this->dotPayIP || $_SERVER['HTTP_X_FORWARDED_FOR'] == $this->dotPayIP);
  }

  private function handleWarning() {
    CRM_Core_Error::debug_log_message('eu.caltha.dotpay warning: Remote address is not allowed, IP:' . $_SERVER['REMOTE_ADDR']);
    CRM_Core_Error::debug_var('eu.caltha.dotpay warning serialized post', serialize($_POST));
    header('HTTP/1.1 200 Ok');
    echo "INVALID_REMOTE_ADDRESS";
    CRM_Utils_System::civiExit();
  }
}
