<?php

require_once 'CRM/Core/Page.php';

class CRM_Dotpay_Page_Final extends CRM_Core_Page {
  function run() {
    CRM_Utils_System::setTitle('Powrót z Dotpay');
    CRM_Dotpay_Tools_Hooks::alterPageRun($this, get_class());
    $this->processStatus();
    $this->processInvoiceId();
    $this->processErrorCode();
    parent::run();
  }

  private function processStatus() {
    $status = CRM_Utils_Request::retrieve('status', 'String', $this, false);
    $this->assign('status', $status);
  }

  private function processInvoiceId() {
    $invoiceId = CRM_Utils_Request::retrieve('control', 'String', $this, false);
    $this->assign('invoiceId', $invoiceId);
    $contribution = new CRM_Dotpay_Tools_Contribution($invoiceId);
    if ($contribution->result) {
      $this->assign('total_amount', $contribution->result->total_amount);
    }
  }

  private function processErrorCode() {
    $errorCode = CRM_Utils_Request::retrieve('error_code', 'String', $this, false);
    $this->assign('errorLabel', '');
    if (in_array($errorCode, CRM_Dotpay_Dicts_ErrorCode::$ids)) {
      $this->assign('errorLabel', CRM_Dotpay_Dicts_ErrorCode::$labels[$errorCode]);
    }
  }
}
