<?php
return array (
  0 => array (
    'name' => 'Dotpay',
    'entity' => 'PaymentProcessorType',
    'params' =>
      array (
        'version' => 3,
        'name' => 'Dotpay',
        'title' => 'Dotpay',
        'description' => 'Dotpay Payment Processor',
        'class_name' => 'Payment_Dotpay',
        'user_name_label' => 'Identyfikator sklepu (#id)',
        'password_label' => 'PIN',
        'billing_mode' => 4, // notify
        'is_default' => 0,
        'is_recur' => 0,
        'payment_type' => 1,
      ),
  ),
  1 => array (
    'module' => 'eu.caltha.dotpay',
    'name' => 'DotpayFinancialType',
    'entity' => 'FinancialType',
    'params' =>
      array (
        'version' => 3,
        'name' => 'Paid by Dotpay',
        'is_deductible' => 0,
        'is_reserved' => 0,
        'is_active' => 1,
      ),
  ),
);
