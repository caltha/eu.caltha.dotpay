<?php

require_once 'CRM/Core/Payment.php';

class CRM_Core_Payment_Dotpay extends CRM_Core_Payment {


  /**
   * We only need one instance of this object. So we use the singleton
   * pattern and cache the instance in this variable
   *
   * @var object
   * @static
   */
  static private $_singleton = NULL;

  /**
   * Mode of operation: live or test.
   *
   * @var object
   */
  protected $_mode = NULL;

  public function __construct($mode, &$paymentProcessor) {
    $this->_mode = $mode;
    $this->_paymentProcessor = $paymentProcessor;
    $this->_processorName = ts('Dotpay');
  }

  public function doTransferCheckout(&$params, $component) {
    $request = CRM_Dotpay_Logic_RequestService::prepare($this->_paymentProcessor, $params);
    $calculatedHash = CRM_Dotpay_Logic_Hash::calculate($this->_paymentProcessor['password'], $request);
    $request = CRM_Dotpay_Logic_RequestService::addChk($request, $calculatedHash);
    $this->redirectPost($this->_paymentProcessor['url_site'], $request);
  }

  /**
   * Calling this from outside the payment subsystem is deprecated - use doPayment.
   *
   * Does a server to server payment transaction.
   *
   * Note that doPayment will throw an exception so the code may need to be modified
   *
   * @param array $params
   *   Assoc array of input parameters for this transaction.
   *
   * @return array
   *   the result in an nice formatted array (or an error object)
   */
  protected function doDirectPayment(&$params) {
    CRM_Core_Error::fatal(ts('This function is not implemented'));
  }

  /**
   * This function checks to see if we have the right config values.
   *
   * @return string
   *   the error message if any
   */
  function checkConfig() {
    $error = array();
    if (empty($this->_paymentProcessor['user_name'])) {
      $error[] = ts('Store ID (id) is not set in the Administer &raquo; System Settings &raquo; Payment Processors');
    }
    if (empty($this->_paymentProcessor['password'])) {
      $error[] = ts('PIN is not set in the Administer &raquo; System Settings &raquo; Payment Processors');
    }
    if (empty($this->_paymentProcessor['url_site'])) {
      $error[] = ts('URL for store is not set in the Administer &raquo; System Settings &raquo; Payment Processors');
    }
    if (!empty($error)) {
      return implode('<p>', $error);
    }
    else {
      return NULL;
    }
  }

  /**
   * Redirect to Dotpay page with data in POST.
   *
   * @param string $url
   * @param array $data
   */
  private function redirectPost($url, array $data) {
    echo '<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
      <script type="text/javascript">
        function closethisasap() {
          document.forms["redirectpost"].submit();
        }
      </script>
    </head>
    <body onload="closethisasap();">
    <form name="redirectpost" method="post" action="' . $url .'">';
    if (!is_null($data)) {
      foreach ($data as $k => $v) {
        echo '<input type="hidden" name="' . $k . '" value="' . $v . '"> ';
      }
    }
    echo '</form>
    </body>
    </html>';
    CRM_Utils_System::civiExit();
  }
}
