{if $status eq 'OK'}
  <p>Dziękujemy za wpłatę na kwotę {$total_amount} zł.</p>
{else}
  <p>Bardzo na przykro, ale wpłata na kwotę {$total_amount} zł nie powiodła się :-(</p>
  {if $errorLabel}<p>{$errorLabel}</p>{/if}
{/if}
